from django.views.generic import TemplateView


class HomePageView(TemplateView):
    template_name = 'index.html'


class CGUPageView(TemplateView):
    template_name = 'cgu.html'