from ._base import *

INSTALLED_APPS += tuple([
    'debug_toolbar',
    'django_extensions',
])

DEBUG = True

MIDDLEWARE += tuple([
    'debug_toolbar.middleware.DebugToolbarMiddleware',
])

# Rajout pour Django Toolbar Debug
DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': lambda x: True,
}

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
