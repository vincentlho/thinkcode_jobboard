from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser

from .vocabularies import CHOICE_DPT, CHOICE_REGIONS, CHOICE_COUNTRIES, BUSINESS_LINE_CHOICES


# from .model_mixins import GetAbsoluteMixin


class Structure(models.Model):
    WORKFORCE_CHOICES = (
        (0, '0'),
        (1, _('1 à 2 salariés')),
        (3, _('3 à 5 salariés')),
        (6, _('6 à 9 salariés')),
        (10, _('10 à 19 salariés')),
        (20, _('20 à 49 salariés')),
        (50, _('50 à 99 salariés')),
        (100, _('100 à 199 salariés')),
        (200, _('200 à 499 salariés')),
        (500, _('500 à 999 salariés')),
        (1000, _('1000 à 1999 salariés')),
        (2000, _('2000 à 4999 salariés')),
        (5000, _('5000 salariés')),
        (5001, _('5000 et plus salariés')),
        (0000, _('inconnu')),

    )

    TURNOVER_CHOICES = (
        (0, '0 - 100 K€'),
        (101, '100 - 250 K€'),
        (251, '250 - 500 K€'),
        (501, '500 - 1 M€'),
        (1001, '1 M€ - 2.5 M€'),
        (2501, '2.5 M€ - 5 M€'),
        (5001, '5 M€ - 10 M€'),
        (10001, 'Supérieur à 10 M€'),
        (0000, 'Autre - précisez'),
    )

    CATEGORY_CHOICES = (
        ('enterprise', _('Entreprise')),
        ('event', _('Événement')),
        ('non_profit_organization', _('Association')),
        ('school', _('École')),
        ('public_institution', _('Collectivité locale')),
    )

    logo = models.ImageField(
        null=True,
        blank=True,
        upload_to='logos/',
        verbose_name=_('Logo'),
    )
    juridic_name = models.CharField(
        max_length=256,
        verbose_name=_('Dénomination sociale'),
    )
    commercial_name = models.CharField(
        null=True,
        blank=True,
        max_length=256,
        verbose_name=_('Libellé commercial'),
    )
    siret = models.CharField(
        max_length=15,
        unique=True,
        verbose_name=_('SIRET')
    )
    num_tva_intracom = models.CharField(
        max_length=15,
        null=True,
        blank=True,
        unique=True,
        verbose_name=_('Numéro TVA intracommunautaire'),
    )
    description = models.TextField(
        max_length=600,  # CDC : max 600 caractères
        null=True,
        blank=True,
        verbose_name=_("Description de l'activité"),
    )
    workforce = models.IntegerField(
        choices=WORKFORCE_CHOICES,
        verbose_name=_('Effectifs'),
    )
    turnover = models.DecimalField(
        choices=TURNOVER_CHOICES,
        decimal_places=2,
        max_digits=15,
        null=True,
        blank=True,
        verbose_name=_("Chiffre d'Affaires"),
    )
    category = models.CharField(
        choices=CATEGORY_CHOICES,
        max_length=25,
        db_index=True,
        verbose_name=_('Catégorie')
    )
    business_line = models.CharField(
        choices=BUSINESS_LINE_CHOICES,
        max_length=100,
        db_index=True,
        verbose_name=_("Secteur d'activité"),
    )
    creation_date = models.DateField(
        auto_now_add=True,
        verbose_name=_('Date de création'),
    )
    address = models.TextField(
        verbose_name=_('Adresse'),
    )
    zipcode = models.CharField(
        max_length=5,
        verbose_name=_('Code Postal'),
    )
    city = models.CharField(
        max_length=60,
        verbose_name=_('Ville'),
    )
    department = models.CharField(
        choices=CHOICE_DPT,
        null=True,
        blank=True,
        max_length=3,
        db_index=True,
        verbose_name=_('Département'),
    )
    region = models.CharField(
        choices=CHOICE_REGIONS,
        null=True,
        blank=True,
        max_length=6,
        db_index=True,
        verbose_name=_('Région'),
    )
    country = models.CharField(
        choices=CHOICE_COUNTRIES,
        max_length=2,
        db_index=True,
        verbose_name=_('Pays'),
    )
    phone1 = models.CharField(
        null=True,
        blank=True,
        max_length=22,
        verbose_name=_('Téléphone fixe'),
    )
    phone2 = models.CharField(
        null=True,
        blank=True,
        max_length=22,
        verbose_name=_('Mobile'),
    )
    phone3 = models.CharField(
        null=True,
        blank=True,
        max_length=22,
        verbose_name=_('Téléphone3'),
    )
    # owners = models.ForeignKey(
    #     'accounts.ThinkcodeUser',
    #     blank=True,
    #     null=True,
    #     on_delete=models.CASCADE,
    #     related_name='owned_structures',
    #     verbose_name=_('Propriétaires'),
    # )

    # def save(self, *args, **kwargs):
    #     super().save(*args, **kwargs)
    #     for owner in self.owners.all():
    #         owner.structures.add(self)

    def __str__(self):
        return "{} - {}".format(self.juridic_name, self.siret or "")

    creation_structure_fields = (
        'logo',
         'juridic_name',
         'commercial_name',
         'address',
         'zipcode',
         'city',
         'country',
         'phone1',
         'phone2',
         'category',
         'workforce',
         'siret',
         'num_tva_intracom',
         'turnover',
         'business_line',
         'description',
    )

    class Meta:
        verbose_name = _('Structure')


class ThinkcodeUser(AbstractUser):
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    email = models.EmailField(_("Adresse email"), unique=True)

    foto = models.ImageField(
        null=True,
        blank=True,
        upload_to='fotos/',
        verbose_name=_('Photo'),
    )
    GENDER_CHOICES = (
        ('F', _('Femme')),
        ('H', _('Homme')),
    )
    gender = models.CharField(
        null=True,
        blank=True,
        verbose_name=_('Civilité'),
        choices=GENDER_CHOICES,
        max_length=1,
    )
    first_name = models.CharField(
        _('first name'), max_length=30
    )
    last_name = models.CharField(
        _('last name'), max_length=150,
    )
    birthday = models.DateField(
        null=True,
        blank=True,
        verbose_name=_('Date de naissance'),
    )
    phone = models.CharField(
        null=True,
        blank=True,
        max_length=22,
        verbose_name=_('Mobile'),
    )
    address = models.TextField(
        null=True,
        blank=True,
        verbose_name=_('Adresse'),
    )
    zipcode = models.CharField(
        null=True,
        blank=True,
        max_length=5,
        verbose_name=_('Code Postal'),
    )
    region = models.CharField(
        choices=CHOICE_REGIONS,
        null=True,
        blank=True,
        max_length=6,
        db_index=True,
        verbose_name=_('Région'),
    )
    city = models.CharField(
        null=True,
        blank=True,
        max_length=60,
        verbose_name=_('Ville'),
    )
    country = models.CharField(
        null=True,
        blank=True,
        max_length=2,
        choices=CHOICE_COUNTRIES,
        verbose_name=_('Pays'),
    )
    # is_qualified = models.BooleanField(
    #     default=False,
    #     verbose_name=_('Abonné Qualifié'),
    # )

    projects = models.TextField(_('projets'), null=True, blank=True, max_length=2000, default='none')

    # cv = models.FileField(_('cv'), null=True, blank=True, upload_to='cv/')

    url_git = models.URLField(null=True, blank=True, verbose_name=_('URL PROFIL GIT'))

    url_site = models.URLField(null=True, blank=True, verbose_name=_('URL SITE RÉALISÉ'))

    structure = models.ForeignKey(
        Structure,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='owner',
        verbose_name=_('Structure'),
    )
    is_employer = models.BooleanField(
        default=False,
        verbose_name=_('Thinkcode User Employer'),
    )

    # @property
    # def active_subscription(self):
    #     return Subscription.active_objects.filter(user=self).first()

    def save(self, *args, **kwargs):
        # self.check_is_qualified()
        super().save(*args, **kwargs)

    # qualified_fields = (
    #     'first_name',
    #     'last_name',
    # )

    creation_employeruser_fields = (
        'foto',
        'gender',
        'first_name',
        'last_name',
        'birthday',
        'phone',
        'address',
        'zipcode',
        'city',
        'country',
        'projects',
        'url_git',
        'url_site',
        'is_employer',
        'email',
        'password1',
        'password2',
    )

    creation_candidateuser_fields = (
        'foto',
        'gender',
        'first_name',
        'last_name',
        'birthday',
        'phone',
        'address',
        'zipcode',
        'city',
        'country',
        'projects',
        'url_git',
        'url_site',
        'is_employer',
        'email',
        'password1',
        'password2',
    )

    update_candidateuser_fields = (
        'foto',
        'gender',
        'first_name',
        'last_name',
        'birthday',
        'phone',
        'address',
        'zipcode',
        'city',
        'country',
        'projects',
        'url_git',
        'url_site',
        'is_employer',
        'email',
    )

    # def check_is_qualified(self):
    #     for field in self.qualified_fields:
    #         if not getattr(self, field, None):
    #             self.is_qualified = False
    #             break
    #     else:
    #         self.is_qualified = True
