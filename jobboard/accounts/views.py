import datetime
from urllib.parse import quote_plus

from django.conf import settings
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth import get_user_model
from django.contrib.auth.views import LoginView as BaseLoginView, PasswordResetView as BasePasswordResetView, \
    PasswordResetConfirmView as BasePasswordResetConfirmView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.urls import reverse, reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render
from django.views.generic import FormView, DetailView, UpdateView, CreateView

from .forms import ThinkcodeUserCreationForm, StructureFormClass
from .models import ThinkcodeUser, Structure


class LoginViews(SuccessMessageMixin, BaseLoginView):
    template_name = 'registration/login.html'
    success_message = _("Vous avez été connecté avec succès")

    def form_valid(self, form):
        response = super().form_valid(form)
        # if not form.get_user().is_qualified:
        #     # If the user has not complete his profile we notify him to do so.
        #     messages.add_message(
        #         self.request,
        #         messages.INFO,
        #         _("Merci de compléter entièrement votre profil pour qu'il soit considéré comme 'qualifié'"),
        #     )
        return response


class UserCreationView(SuccessMessageMixin, FormView):
    template_name = 'registration/signup.html'
    form_class = ThinkcodeUserCreationForm
    success_message = _(
        "Votre compte a été créé")

    def form_valid(self, form):
        user = form.save(commit=False)
        email = user.email
        user.username = email
        user.save()
        # subtype_display=SubscriptionType.objects.first()
        # today = datetime.date.today()
        # Subscription.objects.create(user=user, is_active=True, start_date=today, subscription_type=subtype_display)
        # confirmation_key = user.get_confirmation_key()
        # url = self.request.build_absolute_uri(reverse('user:email_confirm'))
        # url_complete = url + "?token=" + confirmation_key + "&email=" + quote_plus(email)
        # subject, from_email, to = _("[THINKCODE] Merci de confirmer votre email"), settings.THINKCODE_EMAIL_FROM, email
        #
        # text_content = '' \
        #                'Bonjour' \
        #                'Cliquez sur le lien suivant pour terminer votre inscription sur Mixity.' \
        #                'f"{url}?token={confirmation_key}&email={quote_plus(email)}"'
        #
        # html_content = render_to_string('accounts/confirm_email.html', {'url': url_complete})
        #
        # msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        # msg.attach_alternative(html_content, "text/html")
        # msg.send()
        # print(f"Allez à l'adresse {url}?token={confirmation_key}&email={quote_plus(email)}")

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('home')


class ProfileView(LoginRequiredMixin, DetailView):
    model = get_user_model()
    template_name = 'accounts/profile.html'

    def get_object(self, **kwargs):
        return self.request.user


class ProfileUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    template_name = 'accounts/profile_update.html'
    model = ThinkcodeUser
    fields = ThinkcodeUser.update_candidateuser_fields
    success_message = _("Votre profil a été enregistré")

    def get_object(self, **kwargs):
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = self.get_object()
        return context

    def get_success_url(self):
        # if not self.get_object().is_qualified:
        #     messages.warning(
        #         self.request,
        #         _("Merci de compléter entièrement votre profil pour qu'il soit considéré comme \"qualifié\""),
        #     )
        return reverse('user:profile')


class ResetPasswordViews(SuccessMessageMixin, BasePasswordResetView):
    #html_email_template_name = 'registration/password_reset_email.html'
    template_name = 'registration/password_reset.html'
    success_message = _("Mail envoyé !")
    success_url = reverse_lazy('user:password_reset_done')


class PasswordResetConfirmView(BasePasswordResetConfirmView):
    success_url = reverse_lazy('user:password_reset_complete')


class StructureCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Structure
    template_name = 'accounts/structure_create.html'
    success_message = _("Nouvelle structure validée")
    fields = Structure.creation_structure_fields

    def save(self, commit=True):
        instance = super().save(commit=commit)
        self.request.user.structure.add(instance)
        return instance

    def form_valid(self, form):
        response = super(StructureCreateView, self).form_valid(form)
        self.object.save()
        self.request.user.structure.add(self.object)
        return response

    def get_success_url(self):
        return reverse('user:structure_profile')


class StructureProfileView(LoginRequiredMixin, DetailView):
    model = Structure
    template_name = 'accounts/structure_profile.html'

    def get_object(self, **kwargs):
        return self.request.user.structure


class StructureUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Structure
    fields = Structure.creation_structure_fields
    template_name = 'accounts/structure_update.html'
    success_message = _("Vous avez mis à jour la structure")

    def get_object(self, **kwargs):
        return self.request.user.structure

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = self.get_object()
        return context

    def get_success_url(self):
        # if not self.get_object().is_qualified:
        #     messages.warning(
        #         self.request,
        #         _("Merci de compléter entièrement votre profil pour qu'il soit considéré comme \"qualifié\""),
        #     )
        return reverse('user:structure_profile')
