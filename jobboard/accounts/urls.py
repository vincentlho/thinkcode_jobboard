from django.contrib.auth import views as auth_views
from django.urls import path

from accounts.views import LoginViews, UserCreationView, ProfileView, ProfileUpdateView, ResetPasswordViews, \
    PasswordResetConfirmView, StructureCreateView, StructureProfileView, StructureUpdateView

app_name = 'user'

urlpatterns = [
    path('login/', LoginViews.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('signup/', UserCreationView.as_view(), name='signup'),
    path('profile/', ProfileView.as_view(), name='profile'),
    path('profile/update/', ProfileUpdateView.as_view(), name='profile_update'),

    path('structure/create/', StructureCreateView.as_view(), name='structure_create'),
    path('structure/profile/', StructureProfileView.as_view(), name='structure_profile'),
    path('structure/profile/update/', StructureUpdateView.as_view(), name='structure_update'),

    path('password-reset/', ResetPasswordViews.as_view(), name='password_reset'),
    path('password-reset/done', auth_views.PasswordResetDoneView.as_view(),name='password_reset_done'),
    path('reset/<slug:uidb64>/<slug:token>/', PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
]