import unicodedata

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import password_validation

from .models import ThinkcodeUser, Structure


class ThinkcodeUserCreationForm(UserCreationForm):
    # captcha = CaptchaField()
    validate_cgu = forms.BooleanField(
        label=mark_safe("<a href={}>{}</a>".format('/home', _("Accepter les conditions d'utilisation"))),
        required=True,
    )

    class Meta:
        model = ThinkcodeUser
        fields = ThinkcodeUser.creation_candidateuser_fields

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['validate_cgu'].label = mark_safe(
            "{} <a href={}>{}</a>".format(
                _("J'accepte les"), reverse_lazy('cgu'),
                _("conditions générales d'utilisation")
            )
        )


class StructureFormClass(forms.ModelForm):
    class Meta:
        model = Structure
        fields = '__all__'