from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UsernameField
from django.utils.translation import ugettext_lazy as _

from .models import Structure, ThinkcodeUser


class ThinkcodeUserForm(UserChangeForm):
    class Meta:
        model = ThinkcodeUser
        fields = '__all__'
        field_classes = {'username': UsernameField}


class ThinkcodeUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'email', 'password')}),

        (_('Personal info'), {'fields': (
        'foto', 'gender', 'first_name', 'last_name', 'birthday', 'phone',
        'address', 'zipcode', 'region', 'city',
        'country', 'projects', 'url_git', 'url_site', 'structure', 'is_employer',)}),

        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),

        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    form = ThinkcodeUserForm
    # readonly_fields = ('is_qualified',)
    list_display = ['username', 'foto', 'first_name',  'last_name', 'email', 'is_employer']
    autocomplete_fields = ['structure']

    class Meta:
        fields = '__all__'


class StructureAdmin(ModelAdmin):
    search_fields = ['juridic_name', 'commercial_name', 'siret']
    list_display = ['juridic_name', 'siret', 'category']


admin.site.register(Structure, StructureAdmin)
admin.site.register(ThinkcodeUser, ThinkcodeUserAdmin)


