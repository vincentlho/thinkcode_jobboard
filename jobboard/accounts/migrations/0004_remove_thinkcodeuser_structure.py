# Generated by Django 3.0.5 on 2020-04-12 13:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_auto_20200412_1528'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='thinkcodeuser',
            name='structure',
        ),
    ]
