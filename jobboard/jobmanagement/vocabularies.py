OFFER_TYPES = (
    ('cdi','CDI'),
    ('cdd', 'CDD'),
    ('interim', 'Interim'),
    ('freelance', 'Freelance'),
    ('internship', 'Internship'),
)


OFFER_STATUSES= (
    ('draft','Draft'),
    ('published', 'Published'),
    ('closed', 'Closed'),
)


APPLIANCE_STATUSES=(
    ('pending','Pending'),
    ('accepted', 'Accepted'),
    ('rejected', 'Rejected'),
)