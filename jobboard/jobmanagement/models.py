from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from .model_mixins import GetAbsoluteMixin
from .vocabularies import OFFER_STATUSES, OFFER_TYPES, APPLIANCE_STATUSES
from ..accounts.models import Enterprise, CandidateUser


class OfferManager(models.Manager):

    def get_by_natural_key(self, slug):
        queryset=self.get(slug=slug)
        return queryset


# class Offer(GetAbsoluteMixin, models.Model):
#     employer = models.ForeignKey(Enterprise, blank=True, null=True, on_delete=models.CASCADE, verbose_name=_('Employeur'))
#     title = models.CharField(max_length=250, verbose_name=_('Titre'))
#     slug = models.SlugField(max_length=250, null=True, verbose_name=_('Slug'))
#     creation_date = models.DateTimeField(auto_now_add=True, verbose_name=_('Date de création'))
#     start_date = models.DateField(verbose_name=_('Date Début Offre'))
#     duration = models.DurationField(null=True, blank=True, verbose_name=_('Durée'))
#     type = models.CharField(max_length=100, choices=OFFER_TYPES, verbose_name=_('Type'))
#     description = models.TextField(blank=True, verbose_name='Description')
#     city = models.CharField(max_length=250, verbose_name=_('Ville'))
#     postcode = models.CharField(max_length=10, verbose_name=_('Code postal'))
#     status = models.CharField(max_length=100, choices=OFFER_STATUSES, verbose_name=_('Status'))
#     skill = models.CharField(max_length=100, blank=True, verbose_name=_('Compétences recherchées'))
#
#     def __str__(self):
#         return self.title
#
#     def natural_key(self):
#         return [self.slug]
#
#     #  to create automatically the slug when an object is created
#     def save(self, *args, **kwargs):  # à voir ne fonctionne pas en shell
#         if self.id:
#             self.slug = slugify('{}_{}'.format(self.id, self.title))
#             super().save(*args, **kwargs)  # en python3 : plus besoin de répéter la classe et le self
#         else:
#             super().save(*args, **kwargs)
#             self.slug = slugify('{}_{}'.format(self.id, self.title))
#             super().save(*args, **kwargs)
#
#     def get_request_queryset(self, request):
#         # import ipdb
#         # ipdb.set_trace()
#         if request.user.type == 'enterpriseuser':
#             queryset = request.user.enterpriseuser.enterprise.offer_set.all()
#         elif request.user.type == 'candidateuser':
#             queryset = Offer.objects.all()  # TODO: provisoirement
#         else:
#             queryset = Offer.objects.none()
#         return queryset
#
#     class Meta:
#         verbose_name = _('Offer')
#         verbose_name_plural = _('Offers')
#         #permissions = (('view_offer', _('Can view an offer')),)


# class Appliance(models.Model):
#     candidate_user = models.ForeignKey(CandidateUser, blank=True, null=True, on_delete=models.CASCADE, verbose_name=_('Candidate User'))
#     announce = models.ForeignKey(Offer, blank=True, null=True, on_delete=models.CASCADE, verbose_name=_('Offre'))
#     date = models.DateTimeField(auto_now_add=True, verbose_name=_('Date de candidature'))
#     status = models.CharField(max_length=100, choices=APPLIANCE_STATUSES, verbose_name=_('Status'))
#     motivational_text = models.TextField(blank=True, verbose_name='Motivationnal Text')
#
#     def __str__(self):
#         return "Candidature du Candidat : {} sur l'Offre : {}".format(self.candidate_user, self.offer)
#
#     class Meta:
#         verbose_name = _('Appliance')
#         verbose_name_plural = _('Appliances')
#         #permissions = (('view_appliance', _('Can view an appliance')),)

