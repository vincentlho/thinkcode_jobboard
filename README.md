# Best Practices
## Categorize the imports into sections as follows:

# System libraries
### import os
### import re
### from datetime import datetime

# Third-party libraries
### import boto
### from PIL import Image

# Django modules
### from django.db import models
### from django.conf import settings

# Django apps
### from cms.models import NewsArticle
### from . import app_settings


# Requirements
### Install Virtualenv
### $virtualenv -p python3.7 venv
### Activate venv
### $source venv/bin/activate

### Clonage Git Project
### git clone https://gitlab.com/vincentlho/thinkcode_jobboard.git

### Install Requirements
### pip install -r requirements/dev.txt

# Démarrage Django
### $./manage.py makemigrations
### $./manage.py migrate
### $./manage.py runserver

